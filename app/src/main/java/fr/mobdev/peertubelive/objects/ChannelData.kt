package fr.mobdev.peertubelive.objects

import android.os.Parcel
import android.os.Parcelable

class ChannelData(val id: Long, val name: String?): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString()
    ) {
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeLong(id)
        dest.writeString(name)
    }

    companion object CREATOR : Parcelable.Creator<ChannelData> {
        override fun createFromParcel(parcel: Parcel): ChannelData {
            return ChannelData(parcel)
        }

        override fun newArray(size: Int): Array<ChannelData?> {
            return arrayOfNulls(size)
        }
    }

}