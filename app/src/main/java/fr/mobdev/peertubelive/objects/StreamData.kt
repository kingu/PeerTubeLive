package fr.mobdev.peertubelive.objects

import android.os.Parcel
import android.os.Parcelable

class StreamData(val url: String?, val key: String?): Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readString()
    ) {
    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeString(url)
        dest.writeString(key)
    }

    companion object CREATOR : Parcelable.Creator<StreamData> {
        override fun createFromParcel(parcel: Parcel): StreamData {
            return StreamData(parcel)
        }

        override fun newArray(size: Int): Array<StreamData?> {
            return arrayOfNulls(size)
        }
    }

}