package fr.mobdev.peertubelive.objects

import android.os.Parcel
import android.os.Parcelable

class StreamSettings(
    val title: String, val channel: Long, val privacy: Int, val category: Int?, val language: String?, val licence: Int?, val description: String?,
    val comments: Boolean, val download: Boolean, val nsfw: Boolean, val saveReplay: Boolean) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString()!!,
        parcel.readLong(),
        parcel.readInt(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readInt(),
        parcel.readString(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte(),
        parcel.readByte() != 0.toByte()
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(title)
        parcel.writeLong(channel)
        parcel.writeInt(privacy)
        if (category != null) {
            parcel.writeInt(category)
        }
        parcel.writeString(language)
        if (licence != null) {
            parcel.writeInt(licence)
        }
        parcel.writeString(description)
        parcel.writeByte(if (comments) 1 else 0)
        parcel.writeByte(if (download) 1 else 0)
        parcel.writeByte(if (nsfw) 1 else 0)
        parcel.writeByte(if (saveReplay) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<StreamSettings> {
        override fun createFromParcel(parcel: Parcel): StreamSettings {
            return StreamSettings(parcel)
        }

        override fun newArray(size: Int): Array<StreamSettings?> {
            return arrayOfNulls(size)
        }
    }
}