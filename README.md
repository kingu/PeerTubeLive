# PeerTube Live
![Build Status](https://drone.mob-dev.fr/api/badges/Schoumi/PeerTubeLive/status.svg)

Stream your phone camera to a PeerTube instance with this Android app.

  * Multiple instance supported — you can use your own.
  * Configurable stream  options (description, title, visibility, …)
  * Switch between cameras
  * Easy to use


PeerTube, developped by [Framasoft](https://framasoft.org), is the libre and decentralized alternative to video platforms. Join PeerTube at [https://joinpeertube.org](https://joinpeertube.org)

## Licence

The app itself is licensed AGPLv3+
The [RTMP/RTSP library](https://github.com/pedroSG94/rtmp-rtsp-stream-client-java) (embedded in this repository is not in the upstream version yet), available under Apache 2.0 Licence

Support for these to be embedded in a RTMP-RTSP library is planned.

## Help

Help translate the app on Hosted Weblate: [https://hosted.weblate.org/projects/peertube-live](https://hosted.weblate.org/projects/peertube-live)

Donate to fund time spent on the differents apps [![Donate](https://liberapay.com/assets/widgets/donate.svg)](https://liberapay.com/Schoumi/donate)
